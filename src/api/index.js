import axios from 'axios'

const defaultConfig = {
  baseURL: import.meta.env.VITE_VUE_APP_BASE_URL,
  // withCredentials: true,
  headers: {
    accept: 'application/json',
  },
}

const DefaultAPIInstance = axios.create(defaultConfig)

export { DefaultAPIInstance }

export default {
  DefaultAPIInstance,
}
