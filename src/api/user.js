import { DefaultAPIInstance } from '@/api'

function alertsListLoad() {
  return DefaultAPIInstance.get('/api/alerts')
}
function exploreListLoad() {
  return DefaultAPIInstance.get('/api/explore')
}

function accountSettingsLoad() {
  return DefaultAPIInstance.get('/api/explore')
}

function accountSettingsUpdate(payload) {
  return DefaultAPIInstance.patch('/api/explore', payload)
}

function accountSettingsAdd(payload) {
  return DefaultAPIInstance.put('/api/explore', payload)
}

export {
  alertsListLoad,
  exploreListLoad,
  accountSettingsLoad,
  accountSettingsUpdate,
  accountSettingsAdd,
}

export default {
  alertsListLoad,
  exploreListLoad,
  accountSettingsLoad,
  accountSettingsUpdate,
  accountSettingsAdd,
}
