import { DefaultAPIInstance } from '@/api'

function signIn(payload) {
  return DefaultAPIInstance.post('/api/auth/login', payload)
}
function signUp(payload) {
  return DefaultAPIInstance.post('/api/auth/registration', payload)
}
function logOut() {
  return DefaultAPIInstance.post('/api/auth/logout')
}
export { signIn, signUp, logOut }

export default {
  signIn,
  signUp,
  logOut,
}
