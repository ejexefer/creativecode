import { DefaultAPIInstance } from '@/api'

function loadClients() {
  return DefaultAPIInstance.get('/clients.json')
}
export { loadClients }

export default {
  loadClients,
}
