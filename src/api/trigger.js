import { DefaultAPIInstance } from '@/api'

function triggerFormLoad(value) {
  return DefaultAPIInstance.get('/api/trigger/' + value)
}
function triggerToAlert(value, payload) {
  return DefaultAPIInstance.post('/api/trigger/' + value, payload)
}
export { triggerFormLoad, triggerToAlert }

export default {
  triggerFormLoad,
  triggerToAlert,
}
