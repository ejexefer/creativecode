import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useClientsStore = defineStore('clients', () => {
  const clientsStore = ref([])
  const clearClientsStore = computed(() => (clientsStore.value = null))
  function updateClientsStore(value) {
    clientsStore.value = value
  }

  return { clientsStore, clearClientsStore, updateClientsStore }
})
